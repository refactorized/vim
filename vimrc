call pathogen#infect()

set expandtab
set shiftwidth=3
set softtabstop=3

set nocompatible

filetype on
filetype plugin on
filetype indent on

syntax on

"some nutso stuff in our status bar see: http://archive09.linux.com/feature/120126
set statusline=%F%m%r%h%w\ [%Y]\ %03l,%02v\ %p%% 
set laststatus=2

"allows unwritten buffers in hidden windows
set hidden

" Don't update the display while executing macros
set lazyredraw

" let yourself know what mode you're in
set showmode

" Enable enhanced command-line completion. Presumes you have compiled
" with +wildmenu.  See :help 'wildmenu'
set wildmenu

" backspace magic
set backspace=indent,eol,start

" filetype hooking 
au BufNewFile,BufRead *.tmpl set filetype=html
autocmd BufNewFile,BufRead *.json set ft=javascript

"" KEYMAPPING
imap <C-Y><C-Y> <C-O>:call zencoding#expandAbbr(3)<CR><Right>

"" Enable Mouse
set mouse=a

""" COLORS (included by pathogen bundle"

if !empty($ITERM_PROFILE)
   if $ITERM_PROFILE=='FullScreen Solarized'
      set background=dark
      colorscheme solarized
   elseif $ITERM_PROFILE=='FullScreen Solarized Light'
      set background=light
      colorscheme solarized
   endif
else 
   set t_Co=256
   colorscheme herald
endif
     

"show line numbers for the love of god
set nu

