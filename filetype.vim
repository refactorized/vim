if exists("did_load_filetypes")
  finish
endif
augroup filetypedetect
   "filetype au lines here:
   au BufRead,BufNewFile *.less setfiletype less

augroup END
