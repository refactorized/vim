set nocompatible
cd ~/.vim

call pathogen#infect()

set expandtab
set shiftwidth=3
set softtabstop=3

set nocompatible

filetype on
filetype plugin on
filetype indent on

syntax on

"allows unwritten buffers in hidden windows
set hidden

" Don't update the display while executing macros
set lazyredraw

" let yourself know what mode you're in
set showmode

" Enable enhanced command-line completion. Presumes you have compiled
" with +wildmenu.  See :help 'wildmenu'
set wildmenu

" backspace magic
set backspace=indent,eol,start

" filetype hooking 
au BufNewFile,BufRead *.tmpl set filetype=html

